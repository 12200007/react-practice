const express = require('express');
const dotenv = require('dotenv')
const pg = require('pg')
const cors = require('cors')

const app= express();
app.use(cors())
app.use(express.json())

const port=5000
dotenv.config()

const pool = new pg.Pool({
    user: process.env.DB_USER,
    host:process.env.DB_HOST,
    database:process.env.DB_NAME,
    password:process.env.DB_PASSWORD,
    port:process.env.DB_PORT,
})

app.get('/getusers', async (req,res)=>{
    try{
        const result = await pool.query('SELECT * FROM users');
        res.status(201).json({data: result.rows})
    }catch(error){
        console.log(error)
    }
})

app.get('/userbyid/:id', async (req,res)=>{
    try{
        const result = await pool.query('SELECT * FROM users WHERE user_id =$1',[req.params.id])
        res.json({data: result.rows})
    }catch(e){
        console.log(e)
    }
})

app.delete('/deleteuser/:id', async (req,res)=>{

    try{
        const {id} = req.params;
        await pool.query('DELETE FROM users WHERE user_id = $1',[id])
        console.log("DELETED Successfully")
    }catch(e){
        console.log(e)
    }
})
app.post('/adduser', async (req, res)=>{
    try{
        const {name, email, password} = req.body;
        await pool.query('INSERT INTO users (name,email,password) VALUES ($1,$2,$3)',[name,email,password]);

        res.status(200).json({message:'User added successfully'})
    }catch(e){
        console.log(e)
    }
})

app.put("/update/:id", async (req,res)=>{
    try{
        const {name, email, password}= req.body;
        await pool.query('UPDATE users SET name =$1 , email=$2 , password=$3 WHERE user_id = $4',[name,email,password, req.params.id])
    }catch(er){
        console.log(er)
    }
})
app.listen(port,()=>{
    console.log(`Server listening on port ${port}`)
})




